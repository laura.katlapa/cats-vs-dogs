<?php


require '.system/core.php';


// basic routing
$url = explode('?', $_SERVER['REQUEST_URI'], 2)[0];
switch ($url) {
    case '/API/catsvsdogs/json':
        header('content-type: text/plain');
        $api = new api($_GET, $_POST);
        echo json_encode($api->catsvsdogs(), JSON_PRETTY_PRINT);
        exit;

    case '/':
        require '.views/home.php';
        exit;
}


// could not find anything
http_response_code(404);
exit;
