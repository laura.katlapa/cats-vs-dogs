<?php


// show errors in dev
//error_reporting(-1);
//ini_set('display_errors', TRUE);


// load basic info
class core {
    public static $conf;
    public static $db_sql;
}

core::$conf = require __DIR__ . '/conf.php';

core::$db_sql = new PDO(
    'mysql:host=localhost;dbname='.core::$conf['db_sql']['name'].';charset=UTF8',
    core::$conf['db_sql']['user'],
    core::$conf['db_sql']['pass']
);
core::$db_sql->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);


// basic functions
function html($str) {
    return htmlspecialchars($str, ENT_QUOTES | ENT_HTML5);
}

// basic class autoloader
spl_autoload_register(function($class){
    require __DIR__.'/../.classes/'.$class.'.php';
});