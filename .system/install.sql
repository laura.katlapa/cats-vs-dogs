CREATE TABLE `catsvsdogs` (
    `id` int(10) UNSIGNED NOT NULL,
    `type` enum('cat', 'dog') COLLATE ascii_bin NOT NULL,
    `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ascii COLLATE=ascii_bin;

ALTER TABLE `catsvsdogs`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `type+name` (`type`, `name`);

ALTER TABLE `catsvsdogs`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
