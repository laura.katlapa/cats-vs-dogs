let calc_winner = function(){
    let cats = $('div.cat ol li').length;
    let dogs = $('div.dog ol li').length;

    $('form.winner').removeClass('winner');
    if (cats > dogs) {
        $('.cat form').addClass('winner');
    }
    else if (cats < dogs) {
        $('.dog form').addClass('winner');
    }
}


$(function(){
    calc_winner();

    $('section.home form').on('submit', function(e) {
        e.preventDefault();

        let
            post = {
                type: $(this).find('input[name="type"]').val(),
                name: $(this).find('input[name="name"]').val().trim(),
            },
            $form = $(this);

        // fake form submit with ajax
        $.post(
            $form.attr('action'),
            post,
            function(response){
                $('.error').html('');

                if (response.save.status === undefined) {
                    $form.find('.error').text('Server error');
                    return;
                }

                if (response.save.status !== true) {
                    if (response.save.error !== null) {
                        $form.find('.error').text(response.save.error);
                    }
                    return;
                }

                $form.find('input[name="name"]').val('');
                let ol = $form.siblings('ol').html('');
                for (let k in response.list) {
                    let v = response.list[k];
                    if (v.type === post.type) {
                        ol.prepend( // writes in descending order
                            $('<li></li>').text(v.name) // text() prevents html injection
                        );
                    }
                }

                calc_winner();
            },
            'json'
        );
    });
});
