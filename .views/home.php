<?php


$api = new api;
$list = $api->catsvsdogs()['list'];


?><!DOCTYPE html>
<html lang="en"><head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0">

    <title>Cats vs. Dogs</title>

    <link rel="stylesheet" href="//fonts.googleapis.com/css2?family=Montserrat&display=swap">
    <link rel="stylesheet" href="/assets/default.css?1">
</head><body>

<main>
    <section class="home">
        <div class="cat">
            <form action="/API/catsvsdogs/json" method="post">
                <span>Cat</span>
                <input type="hidden" name="type" value="cat">
                <div>
                    <input name="name" placeholder="your cat name" autocomplete="off">
                    <input type="submit" value="add">
                </div>
                <div class="error"></div>
            </form>
            <ol><?php
                foreach (array_reverse($list) as $animal) {
                    if ($animal['type'] === 'cat') {
                        echo '<li>', html($animal['name']), '</li>';
                    }
                }
            ?></ol>
        </div>
        <div class="dog">
            <form action="/API/catsvsdogs/json" method="post">
                <span>Dog</span>
                <input type="hidden" name="type" value="dog">
                <div>
                    <input name="name" placeholder="your dog name" autocomplete="off">
                    <input type="submit" value="add">
                </div>
                <div class="error"></div>
            </form>
            <ol><?php
                foreach (array_reverse($list) as $animal) {
                    if ($animal['type'] === 'dog') {
                        echo '<li>', html($animal['name']), '</li>';
                    }
                }
            ?></ol>
        </div>
    </section>
</main>

<script src="/assets/jquery.min.js"></script>
<script src="/assets/default.min.js"></script>

</body></html>