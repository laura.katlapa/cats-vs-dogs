<?php


class api {
    private $get, $post;


    public function __construct($get = [], $post = []) {
        $this->get = $get;
        $this->post = $post;
    }


    public function catsvsdogs() {
        $return = [
            'save' => [
                'status' => FALSE,
                'error' => NULL,
            ],
            'list' => [],
        ];

        // add a new pet?
        if ($this->post) {
            // check data
            if (
                !array_key_exists('type', $this->post)
                or !in_array($this->post['type'], ['cat', 'dog'], TRUE)
            ) {
                $return['save']['error'] = 'This type of animal is not supported.';
            }
            elseif (
                !array_key_exists('name', $this->post)
                or !$this->post['name'] = trim($this->post['name'])
            ) {
                $return['save']['error'] = 'Name cannot be empty.';
            }

            // search for duplicates first as INSERT IGNORE would increase AUTO_INCREMENT by 1
            else {
                $q = core::$db_sql->prepare('
                    SELECT id
                    FROM `catsvsdogs`
                    WHERE type = :type AND name = :name # UNIQUE KEY
                ');
                $q->execute([
                    'type' => $this->post['type'],
                    'name' => $this->post['name'],
                ]);
                if ($q->rowCount()) {
                    $return['save']['error'] = 'Name already exists.';
                }

                else {
                    // insert
                    core::$db_sql->prepare('
                        INSERT INTO `catsvsdogs` (type, name)
                        VALUES (:type, :name)
                    ')->execute([
                        'type' => $this->post['type'],
                        'name' => $this->post['name'],
                    ]);
                    if (!core::$db_sql->lastInsertId()) {
                        $return['save']['error'] = 'Could not save to DB.';
                    }

                    else {
                        $return['save']['status'] = TRUE;
                    }
                }
            }
        }

        $q = core::$db_sql->query('
            SELECT type, name
            FROM `catsvsdogs`
            ORDER BY id
        ');
        foreach ($q->fetchAll() as $row) {
            $return['list'][] = $row;
        }

        return $return;
    }
}
